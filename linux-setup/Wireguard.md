## Setup new wireguard connection

    - Generate private and public keys:
        cd /etc/wireguard
        wg genkey | tee privatekey | wg pubkey > publickey && chmod 400 privatekey publickey
    - Create file /etc/wireguard/wg0.conf && chmod 600 /etc/wireguard/wg0.conf:
        [Interface]
        Address = 10.8.1.X
        PrivateKey = <generated private key>

        [Peer]
        # server's public key
        PublicKey = PJL6Nf/o1TAWEpV8uk3lGEEZ2M5YvHivAfMuAdNZYDQ=
        Endpoint = bkmks.com:51820
        # NOTE: AllowedIPs act as a routing table when sending, and an ACL when receiving
        AllowedIPs = 10.8.1.0/24, 10.0.0.0/24
        PersistentKeepalive = 25

    - Edit server side /etc/wireguard/wg0.conf to add:
        [Peer]
        PublicKey = <generated public key>
        AllowedIPs = 10.8.1.X

    - Enable and start wg service (on both ends):
        systemctl enable wg-quick@wg0.service
        systemctl start wg-quick@wg0.service
     Note: the above doesn't reliably start WG servicebecause of permanent server name resolution issue,
     to solve this, don't enable the above, instead create executable file /etc/NetworkManager/dispatcher.d/99-wg:
        #!/bin/bash
        ETH=enp3s0
        if [[ "$1" == "$ETH" ]] ; then
            if [[ "$2" = "up" ]] ; then
                /usr/bin/wg-quick up wg0
            elif [[ "$2" = "down" ]] ; then
                /usr/bin/wg-quick down wg0
            fi
        fi
        exit 0

