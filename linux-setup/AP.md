## How to configure one port router

### Configure managed switch

    - assign port 8 to VLAN 2 untagged, this is the WAN port
    - assign port 7 to VLAN 1 tagged and VLAN 2 untagged, this is the router port
    - assign ports 1-6 to VLAN 1 untagged, these are home network ports
    - set ports 7,8 PVID to 2 and ports 1-6 PVID to 1

### Add vlan interfaces for home and WAN sides:

    - Add net.ifnames=0 to kernel parameters, to switch to using conventional dev names, i.e. eth0, wlan0

    - Allow server routing by adding net.ipv4.ip_forward = 1 to /etc/sysctl.conf, and enable the changes:
        sysctl -p /etc/sysctl.conf

    - Prevent NM from managing wlan interface, add to /etc/NetworkManager/NetworkManager.conf:
        [keyfile]
        unmanaged-devices=interface-name:wlp2s0

    - Load 802.1q VLAN module `modprobe 8021q` and persist in /etc/modules-load.d/8021q.conf:
        8021q

    - Add the following to /etc/NetworkManager/dispatcher.d/99-alexnet, to setup vlan1 and add it to a bridge:
        iwconfig $WLAN txpower 14
        iwconfig $WLAN power off

        #ip link set dev $ETH address 90:E6:BA:51:F5:0A <- vsadu
        ip link set dev $ETH address EC:08:6B:AE:B6:98
        ip link add link enp3s0 name vlan1 type vlan id 1
        brctl addbr br0
        brctl addif br0 vlan1
        ip addr add 192.168.0.1/24 dev br0
        ip link set dev vlan1 up
        ip link set dev br0 up
        #iptables -t nat -A POSTROUTING -s 192.168.0.0/24 ! -d 192.168.0.0/24  -j MASQUERADE
        iptables -t nat -A POSTROUTING -o $ETH  -j MASQUERADE

        ip link add link $ETH name vlan1 type vlan id 1
        brctl addbr br0
        brctl addif br0 vlan1
        brctl addif br0 $WLAN
        ip addr add 192.168.0.1/24 dev br0
        ip link set dev vlan1 up
        ip link set dev br0 up
        #iptables -t nat -A POSTROUTING -s 192.168.0.0/24 ! -d 192.168.0.0/24  -j MASQUERADE

    - Customize eth0 MAC by adding to /etc/NetworkManager/system-connections/eth0.nmconnection:
        [ethernet]
        cloned-mac-address=90:E6:BA:51:F5:0A

    -- skip the rest of this section, as using NM for this is unreliable
    - Add tagged vlan interface with enp3s0 parent physical device to represent wired home LAN:
        nmcli con add type vlan ifname vlan1 dev enp3s0 id 1
        #nmcli dev status
        #nmcli con show
        #nmcli con delete UUID
    - Bridge wireless interface and vlan1 and assign it to home LAN subnet:
        nmcli con add type bridge ifname br0 ip4 192.168.0.1/24
        nmcli con up bridge-br0
        nmcli con mod vlan-vlan1 master br0 slave-type bridge
        Note: don't add wlan to the bridge, instead let hostapd add it according to configuration.
        #nmcli con delete <UUID of all existing wireless connections>
        #nmcli con add type bridge-slave ifname wlp2s0 master br0
        #turn off STP: `nmcli con modify br0 bridge.stp no`
    - ?Note: in case of trouble with UDP, lower MTU, e.g. `nmcli connection modify <UUID> mtu 1420`
    - ?Restart networking just in case:
        systemctl restart NetworkManager

### Setup wireless access point

    - Note: sometimes hostapd can trigger kernel bug in connection with debugfs, rendering system unusable.
      In this case disabling selinux should help. To turn off selinux, edit /etc/selinux/config accordingly,
      then reboot and verify: `sestatus`
    - Helpful commands:
        iw list
        iwconfig
        rfkill
        wavemon
    - If hostapd gives 'Could not configure driver mode':
        nmcli radio wifi off
      Now enable radio (it will be enabled after reboot, even though NM doesn't manage the interface otherwise):
        nmcli radio wifi on
        #rfkill unblock wlan
      Configure and start hostapd, named, dhcpd
        systemctl start hostapd
      Check all connections are "green":
        nmcli c s

### Misc
    - See dhcp leases: `cat /var/lib/dhcpd/dhcpd.leases`
    - See associated WIFI devices: `iw dev wlan0 station dump`, or `hostapd_cli all_sta`
    - Open bluetooth screen in settings and learn it's MAC, then use the it to check if device is in range:
        hcitool scan
        sudo hcitool info 88:36:5F:61:B3:1C
        or, sudo l2ping 88:36:5F:61:B3:1C

